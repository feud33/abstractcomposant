import mecanique.Dossier;
import mecanique.Fichier;

public class Main {

    public static void main(String[] args) {
        // déclaration du dossier principal dEntreprise
        Dossier dEntreprise = new Dossier("Entreprise");
        // création et ajout du dossier commercial
        Dossier dCommercial = new Dossier("commercial");
        dEntreprise.ajouter(dCommercial);
        // ajout de sous-dossiers et de fichiers au dossier commercial
        Dossier dClients = new Dossier("clients");
        Fichier fClients = new Fichier("clients");
        dClients.ajouter(fClients);
        Fichier fProspects = new Fichier("prospects");
        dClients.ajouter(fProspects);
        dCommercial.ajouter(dClients);
        // ajout d'un sous-dossier "technique" au dossier principal
        Dossier dTech = new Dossier("technique");
        dTech.ajouter(new Fichier("normes"));
        dTech.ajouter(new Fichier("brevets"));
        dTech.ajouter(new Fichier("produits"));
        dEntreprise.ajouter(dTech);
        // traitement des dossiers
        dEntreprise.traiter();

        /*
        Traitement du dossier Entreprise
        Traitement du dossier commercial
        Traitement du dossier clients
        Traitement du fichier clients
        Traitement du fichier prospects
        Traitement du dossier technique
        Traitement du fichier normes
        Traitement du fichier brevets
        Traitement du fichier produits
         */
    }
}