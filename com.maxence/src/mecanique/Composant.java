package mecanique;

public abstract class Composant {
    String nom;

    public Composant(String nom) {
        this.nom = nom;
    }

    public void traiter() {
        System.out.println(this.toString());
    }
}