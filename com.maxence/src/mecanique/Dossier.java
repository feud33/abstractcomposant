package mecanique;

import java.util.ArrayList;

public class Dossier extends Composant {

    ArrayList<Composant> lesComposants = new ArrayList<Composant>();

    public Dossier(String nom) {
        super(nom);
    }

    public void ajouter(Composant Composant) {
        this.lesComposants.add(Composant);
    }

    public void retirer(Composant Composant) {
        this.lesComposants.remove(Composant);
    }


    @Override
    public void traiter() {
        super.traiter();
        for (Composant composant : this.lesComposants) {
            composant.traiter();
        }
    }

    @Override
    public String toString() {
        return "Traitement du dossier " + nom;
    }
}
