package mecanique;

public class Fichier extends Composant {

    public Fichier(String nom) {
        super(nom);
    }

    @Override
    public String toString() {
        return "Traitement du fichier " + nom;
    }
}
